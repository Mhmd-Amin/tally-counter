package sbu.cs;

import java.util.ArrayList;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long sum = 0;
        for (int i = 0; i < arr.length; i += 2) {
            sum += arr[i];
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int[] arrNumber = new int[arr.length];
        for(int i = arr.length - 1, j = 0; i >= 0; i--, j++) {
            arrNumber[j] = arr[i];
        }
        return arrNumber;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        if (m1[0].length != m2.length) {
            throw new RuntimeException();
        }
        else {
            double[][] m3 = new double[m1.length][m2[0].length];
            for (int rowIndexM1 = 0; rowIndexM1 < m1.length; rowIndexM1++) {
                for (int colIndexM2 = 0; colIndexM2 < m2[0].length; colIndexM2++) {
                    double sum = 0;
                    for (int k = 0; k < m2.length; k++) {
                        sum += (m1[rowIndexM1][k] * m2[k][colIndexM2]);
                    }
                    m3[rowIndexM1][colIndexM2] = sum;
                }
            }
            return m3;
        }
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> words = new ArrayList<List<String>>();
        for (int i = 0; i < names.length; i++) {
            List<String> NamesInRow = new ArrayList<String>();
            for (int j = 0; j < names[i].length; j++) {
                NamesInRow.add(names[i][j]);
            }
            words.add(NamesInRow);
        }
        return words;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        List<Integer> primeSet = new ArrayList<Integer>();
        for (int i = 2; i <= n; i++) {
            if (n % i == 0 && isPrime(i)) {
                primeSet.add(i);
            }
        }
        return primeSet;
    }
    private static boolean isPrime(int num) {
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        String[] arrWords = line.split(" ");
        return strArrToList(arrWords);
    }
    private static List<String> strArrToList(String[] arrWords) {
        List<String> listWords = new ArrayList<String>();
        for (int i = 0; i < arrWords.length; i++) {
            listWords.add(filter(arrWords[i]));
        }
        return listWords;
    }
    private static String filter(String word) {
        for (int i = word.length() - 1; i >= 0; i--) {
            if(!(Character.isDigit(word.charAt(i)) || Character.isAlphabetic(word.charAt(i)))) {
                word = removeChar(i, word);
            }
        }
        return word;
    }
    private static String removeChar(int index, String str) {
        return str.substring(0, index) + str.substring(index + 1);
    }
}
