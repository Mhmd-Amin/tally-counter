package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        long fact = 1;
        for (int i = 1; i <= n; i++) {
            fact *= i;
        }
        return fact;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        long fibo = 1;
        long prevoiusFibo = 0;

        if (n == 1) {
            return fibo;
        }
        for (int i = 2; i <= n; i++) {
            long temp = fibo;
            fibo += prevoiusFibo;
            prevoiusFibo = temp;
        }
        return fibo;
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {
        StringBuilder sb = new StringBuilder(word);
        return sb.reverse().toString();
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        line = line.toLowerCase();
        for (int i = 0, j = line.length() - 1; i != j; i++, j--) {
            while (line.charAt(i) == ' ') {
                i++;
            }
            if (i >= j) {
                break;
            }
            while (line.charAt(j) == ' ') {
                j--;
            }
            if (i >= j) {
                break;
            }
            if (line.charAt(i) != line.charAt(j)) {
                return false;
            }
        }
        return true;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
        int rows = str1.length(), cols = str2.length();
        char[][] board = new char[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if(str1.charAt(i) == str2.charAt(j)) {
                    board[i][j] = '*';
                }
                else {
                    board[i][j] = ' ';
                }
            }
        }
        return board;
    }
}
