package sbu.cs;

import java.util.Random;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        Random rand = new Random();
        String password = new String();
        for (int i = 0; i < length; i++) {
            char letter = (char) (rand.nextInt(26) + 'a');
            password += letter;
        }
        return password;
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        if (length < 3) {
            throw new Exception();
        }
        Random rand = new Random();
        String password = new String();
        char letter;

        letter = (char) (rand.nextInt(6) + '!');
        password += letter;
        letter = (char) (rand.nextInt(10) + '0');
        password += letter;
        letter = (char) (rand.nextInt(26) + 'a');
        password += letter;
        for (int i = 3; i < length; i++) {
            int state = rand.nextInt(3);
            if (state == 0) {
                letter = (char) (rand.nextInt(6) + '!');
            }
            else if (state == 1) {
                letter = (char) (rand.nextInt(10) + '0');
            }
            else {
                letter = (char) (rand.nextInt(26) + 'a');
            }
            password += letter;
        }
        return password;
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    private static int fibo = 1;
    private static int previousFibo = 0;
    public static boolean isFiboBin(int n) {
        int i = 1;
        while (true) {
            int fiboValue = fibonacci();
            int fiboBin = toBinary(fiboValue) + fiboValue;
            if(fiboBin == n) {
                return true;
            }
            else if (n < fiboBin) {
                return false;
            }
            i++;
        }
    }

    private static int fibonacci() {
        int temp = fibo;
        fibo += previousFibo;
        previousFibo = temp;

        return fibo;
    }

    private static int toBinary(int num) {
        int remainder, i = 1, binaryNum = 0;

        while (num != 0){
            remainder = num % 2;
            num /= 2;
            binaryNum += remainder * i;
            i *= 10;
        }
        return binaryNum;
    }
}
