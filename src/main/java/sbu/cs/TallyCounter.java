package sbu.cs;

public class TallyCounter implements TallyCounterInterface {
    private int counter;

    public TallyCounter() {
        counter = 0;
    }

    @Override
    public void count() {
        if (counter <= 9998) {
            counter++;
        }
    }

    @Override
    public int getValue() {
        return counter;
    }

    @Override
    public void setValue(int value) throws IllegalValueException {
        if (value < 0 || 9999 < value) {
            throw new IllegalValueException();
        }
        else {
            counter = value;
        }
    }
}
